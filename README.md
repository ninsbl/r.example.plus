# r.example.plus: GRASS GIS module example in Python

An example of a GRASS GIS module showing:
* which files to include,
* how to script raster map processing,
* how to handle parameters,
* how to publish documentation as a website, and
* how to describe everything.

## How to use this project

When using GitLab:

* Fork the repository using the Fork button.
* Go to Settings > General > Advanced and press Remove fork relationship.
* Go to Settings > General > Rename project and repository and change description
* Go to CI/CD > Run pipeline > Create pipeline to activate CI

When not using GitLab:

* Just download the files or clone the repository using Git.

## Files which usually are not part of a module

These are the files in this repository which usually are not part of
a GRASS GIS module source code, but are useful for a standalone repository.

* README (README.md) is very useful for a standalone repository,
  but is not required for a GRASS GIS module because installation,
  code contributions, etc. are already described in the main repository.
* LICENSE file makes it easier to identify the license (even when the
  license is specified elsewhere). It is not required for the modules
  in the main repository as there is a license file already included.
* .gitlab-ci.yml file is for GitLab Continuous Integration.

## What is next

Consider contributing the your module to the
[GRASS GIS Addons repository](https://grass.osgeo.org/development/code-submission/).
This comes with several advantages such as maintenance support
from the core team and easier distribution to users.
At the same time, you can still keep using
repository like this one for development and release the
code to the Addons when ready. This is especially suitable for
for larger projects with multiple contributors.
For smaller projects, it may be more suitable to do initial
development separately, but after maturing, move the code Addons.

## How to contribute to this repository

Fork the project and submit a merge request or open an issue.